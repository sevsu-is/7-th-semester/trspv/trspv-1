#include <iostream>
#include <iomanip>
#include "mpich-x86_64/mpi.h"
#include <math.h>

const int matrixSize = 3;

void fillMatrix(int matrix[matrixSize][matrixSize], int size) {
    std::cout << "Please fill in a matrix: " << std::endl;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            std::cin >> matrix[i][j];
        }
    }
}

template <typename T>
void printMatrix(T matrix[matrixSize][matrixSize], int size) {
    std::cout << "Matrix: " << std::endl;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            std::cout << std::setw(6) << std::setprecision(2) << matrix[i][j] << " ";
        }
        std::cout << std::endl;
    }
}


void calculateMinor(int matrix[matrixSize][matrixSize], int row, int col, int minorMatrix[matrixSize][matrixSize][matrixSize-1][matrixSize-1]) {
    int k = 0;
    for (int i = 0; i < matrixSize; i++) {
        if (i == row)
            continue;
        int l = 0;
        for (int j = 0; j < matrixSize; j++) {
            if (j == col)
                continue;
            minorMatrix[row][col][k][l] = matrix[i][j];
            l++;
        }
        k++;
    }
}

int calculate3dDeterminant(int matrix[3][3]) {
    return matrix[0][0] * matrix[1][1] * matrix[2][2] +
           matrix[0][1] * matrix[1][2] * matrix[2][0] +
           matrix[0][2] * matrix[1][0] * matrix[2][1] -
           matrix[2][0] * matrix[1][1] * matrix[0][2] -
           matrix[2][1] * matrix[1][2] * matrix[0][0] -
           matrix[2][2] * matrix[1][0] * matrix[0][1];
}

int calculate2dDeterminant(int matrix[2][2]) {
    return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
}


void calculateInverseMatrix(int minors[matrixSize][matrixSize][matrixSize-1][matrixSize-1], int determinant, double inverseMatrix[matrixSize][matrixSize]) {
    if (determinant == 0) {
        std::cout << "Matrix with zero determinant does not have the inverse matrix" << std::endl;
        return;
    }

    for (int i = 0; i < matrixSize; i++) {
        for (int j = 0; j < matrixSize; j++) {
            inverseMatrix[j][i] = (std::pow(-1, (i + 1) + (j + 1))  * calculate2dDeterminant(minors[i][j])) / (double) determinant;
        }
    }
}


int main(int argc, char *argv[]) {
    int rank;
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int transferSize = matrixSize * matrixSize + 1;
    int matrix[matrixSize][matrixSize] = {};
    int minorsMatrix[matrixSize][matrixSize][matrixSize-1][matrixSize-1] = {};
    double inverseMatrix[matrixSize][matrixSize] = {};

    if (rank == 0) {
        fillMatrix(matrix, matrixSize);
        MPI_Send(matrix, transferSize, MPI_INT, 1, 99,MPI_COMM_WORLD);
        MPI_Send(matrix, transferSize, MPI_INT, 2, 99,MPI_COMM_WORLD);
        MPI_Recv(inverseMatrix, matrixSize * matrixSize + 1, MPI_DOUBLE, 2, 99,MPI_COMM_WORLD, &status);
        printMatrix(inverseMatrix, matrixSize);
    } else if (rank == 1) {
        MPI_Recv(matrix, transferSize, MPI_INT, 0, 99, MPI_COMM_WORLD, &status);

        std::cout << std::endl;
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                calculateMinor(matrix, i, j, minorsMatrix);
                std::cout << "Minors for ("  << i << ", " << j << ")" << std::endl;
                for (int k = 0; k < matrixSize - 1; k++) {
                    for (int l = 0; l < matrixSize - 1; l++) {
                        std::cout << minorsMatrix[i][j][k][l] << ", ";
                    }
                    std::cout << std::endl;
                }
                std::cout << std::endl;
            }
        }
        MPI_Send(minorsMatrix, matrixSize * matrixSize * (matrixSize - 1) * (matrixSize - 1) + 1, MPI_INT, 2, 99, MPI_COMM_WORLD);
    } else if (rank == 2) {
        MPI_Recv(matrix, transferSize, MPI_INT, 0, 99, MPI_COMM_WORLD, &status);
        int determinant = calculate3dDeterminant(matrix);

        MPI_Recv(minorsMatrix, matrixSize * matrixSize * (matrixSize - 1) * (matrixSize - 1) + 1, MPI_INT, 1, 99, MPI_COMM_WORLD, &status);
        calculateInverseMatrix(minorsMatrix, determinant, inverseMatrix);
        MPI_Send(inverseMatrix, matrixSize * matrixSize + 1, MPI_DOUBLE, 0, 99,MPI_COMM_WORLD);
    }
    MPI_Finalize();
}